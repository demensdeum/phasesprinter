from time import sleep
from time import time
from os import system

class Sprint:
    def __init__(self, name, startPhase):
        self.name = name
        self.startPhase = startPhase

class Phase:
    def __init__(self, name, period, message, command):
        self.name = name
        self.period = period
        self.message = message
        self.command = command

class Sprinter:
    def __init__(self, sprint):
        self.sprint = sprint
        self.startPhase = sprint.startPhase

    def start(self):
        self._switchPhase(self.startPhase)
        self._run()

    def _switchPhase(self, phase):
        self.phase = phase
        self.endDate = time() + phase.period
        system(phase.command)

    def _run(self):
        while True:
            phase = self.phase
            endDate = self.endDate
            if self.endDate < time():
                nextPhase = self.phase.nextPhase
                if nextPhase == None:
                    print("finished!")
                    exit(0)
                else:
                    print(f"Switching to {nextPhase.name}")
                    self._switchPhase(nextPhase)
            else:
                sleep(1)
                diff = int(endDate - time()) / 60
                print(f"Phase: {phase.name} - {phase.message} ({diff})")


def videoCourseSprint():
    longRelaxMinutes = 4
    shortRelaxMinutes = 2
    workMinutes = 4

    longRelaxPhase = Phase("Long Relax", longRelaxMinutes * 60, "Relax...", "spd-say \"Relax bro!\" && firefox https://c0.cprnt.com/storage/p/t1/am/lwh/f6/6f5/69de2ccb15f250104939592bf8e/main.png")
    shortRelaxPhase = Phase("Short Relax", shortRelaxMinutes * 60, "Relax...", "spd-say \"Relax bro!\" && firefox https://shop.prophotoasia.com/wp-content/uploads/2019/06/Blue-sea-relax.jpg")
    workPhaseOne = Phase("Work", workMinutes * 60, "Work...", "spd-say \"Work bro!\" && firefox http://risovach.ru/upload/2014/01/mem/plyushevaya-boroda_41157834_orig_.jpeg")
    workPhaseTwo = Phase("Work", workMinutes * 60, "Work...", "spd-say \"Work bro!\" && firefox http://risovach.ru/upload/2014/01/mem/plyushevaya-boroda_41157834_orig_.jpeg")

    longRelaxPhase.nextPhase = workPhaseOne
    workPhaseOne.nextPhase = shortRelaxPhase
    shortRelaxPhase.nextPhase = workPhaseTwo
    workPhaseTwo.nextPhase = longRelaxPhase

    return Sprint("Video Course Sprint", longRelaxPhase)

def codingSprint():
    relaxMinutes = 5
    workMinutes = 10

    relaxPhase = Phase("Relax", relaxMinutes * 60, "Relax...", "spd-say \"Relax bro!\" && firefox https://c0.cprnt.com/storage/p/t1/am/lwh/f6/6f5/69de2ccb15f250104939592bf8e/main.png")
    workPhase = Phase("Work", workMinutes * 60, "Work...", "spd-say \"Work bro!\" && firefox http://risovach.ru/upload/2014/01/mem/plyushevaya-boroda_41157834_orig_.jpeg")

    relaxPhase.nextPhase = workPhase
    workPhase.nextPhase = relaxPhase

    return Sprint("Coding Sprint", relaxPhase)

sprint = videoCourseSprint()
sprinter = Sprinter(sprint)
sprinter.start()
